package aplicacion;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import org.junit.Test;

public class TestComparador {
	@Test
	public void ComparadorIgual() {
		ArrayList<Jugador> prueba = new ArrayList<Jugador>();
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Jugador Lanzini = new Jugador("Lanzini" , "Argentina");
		Messi.setearCantGoles(10);
		Messi.setearCantFaltas(10);
		Messi.setearCantTarjetas(0);
		Messi.setearPuntajePromedio(8);
		Messi.sacarCoeficienta();
		Lanzini.setearCantGoles(10);
		Lanzini.setearCantFaltas(10);
		Lanzini.setearCantTarjetas(0);
		Lanzini.setearPuntajePromedio(8);
		Lanzini.sacarCoeficienta();
		prueba.add(Messi);
		prueba.add(Lanzini);
		assertEquals(Comparador.porCoeficiente().compare(Lanzini, Messi),0);
	}
	
	@Test
	public void ComparadorMenor() {
		ArrayList<Jugador> prueba = new ArrayList<Jugador>();
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Jugador Lanzini = new Jugador("Lanzini" , "Argentina");
		Messi.setearCantGoles(10);
		Messi.setearCantFaltas(10);
		Messi.setearCantTarjetas(0);
		Messi.setearPuntajePromedio(8);
		Messi.sacarCoeficienta();
		Lanzini.setearCantGoles(8);
		Lanzini.setearCantFaltas(8);
		Lanzini.setearCantTarjetas(0);
		Lanzini.setearPuntajePromedio(7);
		Lanzini.sacarCoeficienta();
		prueba.add(Messi);
		prueba.add(Lanzini);
		assertEquals(Comparador.porCoeficiente().compare(Lanzini, Messi),1);
	}
	
	@Test
	public void ComparadorMayor() {
		ArrayList<Jugador> prueba = new ArrayList<Jugador>();
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Jugador Lanzini = new Jugador("Lanzini" , "Argentina");
		Messi.setearCantGoles(10);
		Messi.setearCantFaltas(10);
		Messi.setearCantTarjetas(0);
		Messi.setearPuntajePromedio(8);
		Messi.sacarCoeficienta();
		Lanzini.setearCantGoles(8);
		Lanzini.setearCantFaltas(8);
		Lanzini.setearCantTarjetas(0);
		Lanzini.setearPuntajePromedio(7);
		Lanzini.sacarCoeficienta();
		prueba.add(Messi);
		prueba.add(Lanzini);
		assertEquals(Comparador.porCoeficiente().compare(Messi,Lanzini),-1);
	}
}
