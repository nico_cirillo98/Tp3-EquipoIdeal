package aplicacion;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import org.junit.Test;

public class TestEquipoIdeal {
	@Test
	public void Mejores4deAlemania() {
		Jugador Neuer = new Jugador ("Neuer" , "Alemania");
		Jugador Hector = new Jugador ("Hector" , "Alemania");
		Jugador Hummels = new Jugador ("Hummels" , "Alemania");
		Jugador Boateng = new Jugador ("Boateng" , "Alemania");
		Jugador Kimmich = new Jugador ("Kimmich" , "Alemania");
		Jugador Kroos = new Jugador ("Kroos" , "Alemania");
		Jugador Khedira = new Jugador ("Khedira" , "Alemania");
		Jugador Draxler = new Jugador ("Draxler" , "Alemania");
		Jugador Ozil = new Jugador ("Ozil" , "Alemania");
		Jugador Muller = new Jugador ("Muller" , "Alemania");
		Jugador Werner = new Jugador ("Werner" , "Alemania");
	
		Neuer.setearPosiciones("Arquero", null);Neuer.setearCantFaltas(0);Neuer.setearCantGoles(0);Neuer.setearCantTarjetas(0);Neuer.setearPuntajePromedio(9);Neuer.sacarCoeficienta();
		Hector.setearPosiciones("Lateral izquierdo", "Central izquierdo");Hector.setearCantFaltas(6);Hector.setearCantGoles(0);Hector.setearCantTarjetas(4);Hector.setearPuntajePromedio(7);Hector.sacarCoeficienta();
		Hummels.setearPosiciones("Central izquierdo", null);Hummels.setearCantFaltas(2);Hummels.setearCantGoles(0);Hummels.setearCantTarjetas(1);Hummels.setearPuntajePromedio(5);Hummels.sacarCoeficienta();
		Boateng.setearPosiciones("Central derecho", "Lateral derecho");Boateng.setearCantFaltas(0);Boateng.setearCantGoles(0);Boateng.setearCantTarjetas(3);Boateng.setearPuntajePromedio(7);Boateng.sacarCoeficienta();
		Kimmich.setearPosiciones("Lateral derecho",null);Kimmich.setearCantFaltas(1);Kimmich.setearCantGoles(0);Kimmich.setearCantTarjetas(1);Kimmich.setearPuntajePromedio(6);Kimmich.sacarCoeficienta();
		Kroos.setearPosiciones("Volante izquierdo", null);Kroos.setearCantFaltas(0);Kroos.setearCantGoles(1);Kroos.setearCantTarjetas(2);Kroos.setearPuntajePromedio(6);Kroos.sacarCoeficienta();
		Khedira.setearPosiciones( "Volante central","Volante derecho");Khedira.setearCantFaltas(1);Khedira.setearCantGoles(0);Khedira.setearCantTarjetas(0);Khedira.setearPuntajePromedio(7);Khedira.sacarCoeficienta();
		Draxler.setearPosiciones("Puntero izquierdo", null);Draxler.setearCantFaltas(2);Draxler.setearCantGoles(3);Draxler.setearCantTarjetas(2);Draxler.setearPuntajePromedio(6);Draxler.sacarCoeficienta();
		Ozil.setearPosiciones("Puntero derecho", null);Ozil.setearCantFaltas(1);Ozil.setearCantGoles(2);Ozil.setearCantTarjetas(1);Ozil.setearPuntajePromedio(7);Ozil.sacarCoeficienta();
		Muller.setearPosiciones("Puntero derecho", null);Muller.setearCantFaltas(2);Muller.setearCantGoles(0);Muller.setearCantTarjetas(1);Muller.setearPuntajePromedio(5);Muller.sacarCoeficienta();
		Werner.setearPosiciones("Centro delantero", null);Werner.setearCantFaltas(3);Werner.setearCantGoles(2);Werner.setearCantTarjetas(0);Werner.setearPuntajePromedio(7);Werner.sacarCoeficienta();
		
		Seleccion Alemania = new Seleccion("Alemania");
		Alemania.agregarJugador(Neuer);
		Alemania.agregarJugador(Hector);
		Alemania.agregarJugador(Hummels);
		Alemania.agregarJugador(Boateng);
		Alemania.agregarJugador(Kimmich);
		Alemania.agregarJugador(Kroos);
		Alemania.agregarJugador(Khedira);
		Alemania.agregarJugador(Draxler);
		Alemania.agregarJugador(Ozil);
		Alemania.agregarJugador(Muller);
		Alemania.agregarJugador(Werner);
		
		ArrayList <Seleccion> a = new ArrayList<>();
		a.add(Alemania);
		
		EquipoIdeal e = new EquipoIdeal(a);
		assertEquals(e.posiciones.size(),4);
	}
	
	@Test
	public void HayEquipo() {
		ArrayList<Seleccion> todas = new ArrayList<>();
		todas.add(Seleccion.leerJson("Selecci�n Argentina"));
		todas.add(Seleccion.leerJson("Selecci�n Brasil"));
		todas.add(Seleccion.leerJson("Selecci�n Espa�ola"));
		EquipoIdeal equipo = new EquipoIdeal(todas);	
		assertEquals(equipo.posiciones.size(),11);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void NoHayEquipo() {
		ArrayList<Seleccion> todas = new ArrayList<>();
		@SuppressWarnings("unused")
		EquipoIdeal equipo = new EquipoIdeal(todas);	
	}
	
}
