package aplicacion;

import java.util.Comparator;

public class Comparador{
	
	public static Comparator<Jugador> porCoeficiente(){
		return new Comparator<Jugador>(){
			@Override
			public int compare(Jugador j, Jugador k){
				if(j.devolverCoeficiente() < k.devolverCoeficiente())
					return 1;
				else if(j.devolverCoeficiente() == k.devolverCoeficiente())
					return 0;
				else
					return -1;
			}
        };		
	}
}