package aplicacion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class Seleccion {
	public String nombre;
	public ArrayList<Jugador> seleccion;
	
	public Seleccion(String nombre) {
		this.nombre = nombre;
		this.seleccion = new ArrayList<>();
		
	}
	
	public String devolverNombre() {
		return nombre;
}
	
	public boolean agregarJugador(Jugador j) {
		if(!j.equals(null)) {
			seleccion.add(j);
			return true;
		}
		return false;
	}
	
	
	public void generarJSON(String nombre)	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		try{
			FileWriter grabar = new FileWriter(nombre);
			grabar.write(json);
			grabar.close();
			
		}
		
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public static Seleccion leerJson(String archivo)
	{
		Gson gson= new Gson();
		Seleccion ret= null;
		
		try
		{
			BufferedReader leer= new BufferedReader(new FileReader(archivo));
			ret= gson.fromJson(leer, Seleccion.class);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ret;
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<Jugador> devolverSeleccion(){
		return (ArrayList<Jugador>) seleccion.clone();
	}
	
	public int tama�o()
	{
		return seleccion.size();
	}
}
