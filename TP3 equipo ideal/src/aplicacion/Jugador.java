package aplicacion;

public class Jugador {
	private String nombre;
	private String nacionalidad;
	private String [] posiciones = new String [2]; //quise hacerlo con ArrayList pero me daba problema para agregar posiciones :(
	private int cantidadGoles;
	private int cantidadFaltas;
	private int tarjetasRecibidas;
	private int puntajePromedio;
	private int coeficiente;
	
	public Jugador (String nombre , String nacionalidad) {
		this.nombre = nombre;
		this.nacionalidad = nacionalidad;
	}
	
	public String devolverNombre() {
		return nombre;
	}
	
	//si solo tiene una posicion,setear null
	public void setearPosiciones(String posicion1 , String posicion2) {
		if(posicion1 != null) {
			posiciones[0] = posicion1;
		}
		if(posicion2 != null) {
			posiciones[1] = posicion2;
		}
		if(posicion1 == null && posicion2 == null) {
			throw new IllegalArgumentException("El jugador tiene que tener posicion para jugar!");

		}
	}
	
	public void setearCantGoles(int goles) {
		if(goles >= 0) {
			this.cantidadGoles = goles;
		}
		else {
			throw new IllegalArgumentException("La cantidad de goles no puede ser menor a cero");
		}
	}
	
	public void setearCantFaltas(int faltas){
		if(faltas >= 0) {
			this.cantidadFaltas = faltas;
		}
		else {
			throw new IllegalArgumentException("La cantidad de faltas no puede ser menor a cero");
		}
	}
	
	public void setearCantTarjetas(int tarjetas) {
			if(tarjetas >= 0) {
				this.tarjetasRecibidas = tarjetas;
			}
			else {
				throw new IllegalArgumentException("La cantidad de tarjetas recibidas no puede ser menor a cero");
			}
		}
	
	public void setearPuntajePromedio(int puntaje) {
		if(puntaje >= 0) {
			this.puntajePromedio = puntaje;
		}
		else {
			throw new IllegalArgumentException("La cantidad de puntaje  no puede ser menor a cero");
		}
	}
	
	public void sacarCoeficienta() {
		this.coeficiente = (2 * cantidadGoles) - (cantidadFaltas/10) - tarjetasRecibidas + puntajePromedio;
	}
	
	public String [] devolverPosiciones() {
		return posiciones;
	}
	
	public String devolverPosicion(int i) {
		if(i==1 || i==2) {
			return posiciones[i-1];
		}
		throw new IllegalArgumentException("El numero de posicion debe ser 1 o 2");
	}
	
	public int devolverCoeficiente() {
		return coeficiente;
	}
	
	public int devolverCantGoles() {
		return cantidadGoles;
	}
	
	public int devolverCantTarjetas() {
		return tarjetasRecibidas;
	}

	public String devolverNacionalidad() {
		return nacionalidad;
	}
}
