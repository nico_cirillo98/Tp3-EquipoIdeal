package aplicacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestSeleccion {
	@Test
	public void CrearJugador() {
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Seleccion prueba = new Seleccion("prueba");
		assertEquals(prueba.agregarJugador(Messi),true);
	}
	
	@Test
	public void TamañoSeleccion() {
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Jugador Lanzini = new Jugador("Lanzini" , "Argentina");
		Seleccion prueba = new Seleccion("prueba");
		prueba.agregarJugador(Messi);
		prueba.agregarJugador(Lanzini);
		assertEquals(prueba.tamaño(),2);
	}
	
	@Test
	public void NombreSeleccion() {
		Seleccion prueba = new Seleccion("prueba");
		assertEquals(prueba.devolverNombre(),"prueba");
	}
}
