package aplicacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestJugador {
	@Test(expected = IllegalArgumentException.class)
	public void JugadorSinPosicion() {
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Messi.setearPosiciones(null, null);
	}
	
	@Test
	public void JugadorConUnaPosicion() {
		
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Messi.setearPosiciones("Extremo Derecho", null);
		assertEquals(Messi.devolverPosiciones()[0].equals("Extremo Derecho"), true);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void JugadorGolesNegativos() {
	
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Messi.setearCantGoles(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void JugadorFaltasNegativas() {
		
		Jugador Messi = new Jugador("Messi" , "Argentina");
		Messi.setearCantFaltas(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void JugadorTarjetasNegativas() {
		
		Jugador Messi = new Jugador("Messi" , "Argentina");
		
		Messi.setearCantTarjetas(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void JugadorPuntajePromedioNegativo() {

		Jugador Messi = new Jugador("Messi" , "Argentina");
		
		Messi.setearPuntajePromedio(-1);
	}
	
	@Test
	public void CoeficienteJugador() {

		Jugador Messi = new Jugador("Messi" , "Argentina");
		Messi.setearCantGoles(10);
		Messi.setearCantFaltas(10);
		Messi.setearCantTarjetas(0);
		Messi.setearPuntajePromedio(8);
		Messi.sacarCoeficienta();
		
		assertEquals(Messi.devolverCoeficiente()==27,true);
	}
	
}
