package aplicacion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class EquipoIdeal {
	private int cantTarjetas;
	private int cantSinGoles;
	private ArrayList<Jugador> todosLosJugadores;
	public static HashMap<Jugador,String> formacion;
	public ArrayList<String> posiciones;
	
	public EquipoIdeal(ArrayList<Seleccion> Selecciones){
		cantTarjetas = 0;
		cantSinGoles = 0;
		todosLosJugadores = new ArrayList<>();
		formacion = new HashMap<>();
		posiciones = new ArrayList<>();
		if(Selecciones.isEmpty()) {
			throw new IllegalArgumentException("Tenes que agregar selecciones!");
		}
		inicializarLista(Selecciones);
		ordenarJugadores();
		agregarAlMejor();
		generarFormacion();
		
	}

	public void agregarAlMejor() {
		formacion.put(todosLosJugadores.get(0), todosLosJugadores.get(0).devolverPosicion(1));
	}
	
	private void inicializarLista(ArrayList<Seleccion> Selecciones) {
		for(Seleccion s : Selecciones) {
			todosLosJugadores.addAll(s.devolverSeleccion());
		}
	}
	
	private void ordenarJugadores() {
		Collections.sort(todosLosJugadores, Comparador.porCoeficiente());
	}
	
	private void generarFormacion() {
		for(Jugador j : todosLosJugadores) {
			if(entra(j)==1) {
				if(revisarRestricciones(j)) {
					formacion.put(j,j.devolverPosicion(1));	
					posiciones.add(j.devolverPosicion(1));
				}
			}
			else {
				if(entra(j)==2) {
					if(revisarRestricciones(j)) {
						formacion.put(j,j.devolverPosicion(2));
						posiciones.add(j.devolverPosicion(2));
					}	
				}
			}
		}
	}
	
	private int entra(Jugador j) {
		if(formacion.size()<11) {
			if(existePosicion(j.devolverPosicion(1))) {
				if(existePosicion(j.devolverPosicion(2))) {
					return 0;
				}
				else {
					return 2;
				}
			}
			else{
				return 1;
			}
		}
		return 0;
	}
	
	public boolean existePosicion(String posicion) {
		if(posiciones.contains(posicion) || posicion==null) {
			return true;
		}
		return false;
	}
	
	public boolean revisarRestricciones(Jugador x) {
		if(contadorNacionalidad(x.devolverNacionalidad()) < 4) {
			if(chequearGoles(x)) {
				if(chequearTarjetas(x)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean chequearGoles(Jugador x) {
		if(x.devolverCantGoles()==0) {
			if(cantSinGoles<6) {
				cantSinGoles++;
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return true;
		}
	}
	
	public boolean chequearTarjetas(Jugador x) {
		if(x.devolverCantTarjetas()>=1) {
			if(cantTarjetas<5) {
				cantTarjetas++;
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return true;
		}
	}
	
	public int contadorNacionalidad(String nacionalidad) {
		int cont=0;
		for(Jugador x : formacion.keySet()) {
			if(x.devolverNacionalidad().equals(nacionalidad)) {
				cont++;
			}
		}
		return cont;
	}
	
	public static String devolverNombreEnPosicion(String posicion) { 
		
		if(formacion.containsValue(posicion)) {
			for(HashMap.Entry<Jugador,String> entry : formacion.entrySet()){
				if(entry.getValue().equals(posicion))
				{
					return entry.getKey().devolverNombre();
				}
				
				
			}
		}
		throw new IllegalArgumentException("No existe jugador para esa posicion!");
	}
	
	public static int coeficientEquipo()
	{
		int cont=0;
		for(HashMap.Entry<Jugador,String> entry : formacion.entrySet())
		{
			cont=cont+entry.getKey().devolverCoeficiente();
		}
		return cont;
	}
		
}
