package Presenter;

import java.util.ArrayList;

import Interfaz.AgregarJugador;
import aplicacion.EquipoIdeal;
import aplicacion.Jugador;
import aplicacion.Seleccion;

public class presenter {

	Jugador jug;
	Seleccion seleccion= new Seleccion(AgregarJugador.seleccionado);
	ArrayList<Seleccion> todas = new ArrayList<>();
	
	public void agregarJugador(String pais, String nombre, String posicion1, String posicion2, int goles, int faltas, int tarjetas, int puntaje) {
		
		String pos1=chquearPosicion(posicion1);
		String pos2=chquearPosicion(posicion2);
		jug= new Jugador(nombre, pais);
		jug.setearPosiciones(pos1, pos2);
		jug.setearCantFaltas(faltas);
		jug.setearCantGoles(goles);
		jug.setearCantTarjetas(tarjetas);
		jug.setearPuntajePromedio(puntaje);
		jug.sacarCoeficienta();

		seleccion.agregarJugador(jug);
		controlarseleccion();
	}

	private void controlarseleccion() 
	{
		if(seleccion.tama�o()==11)
		{
			seleccion.generarJSON(AgregarJugador.seleccionado);
		}
	}

	// en caso de que tenga una sola posicion transforma el String a null
	private String chquearPosicion(String posicion1) 
	{
		if(posicion1 != "")
		{
			return posicion1;
		}
		else
		{
			return null;
		}
		
	}

	//controla que se seten los 11 jugadores
	public boolean controlarContador(Integer cont) 
	{
		if(cont== 11)
		{
			return false;
		}
	
		return true;		
	}

	//una vez seteados los 11 jugadores, se habilita el boton para crear a la seleccion ideal
	public boolean habilitar(Boolean control) 
	{
		if (control == false)
		{
			return true;
		}
		return false;
	}

	public String setEquipo(String posicion) 
	{
		return EquipoIdeal.devolverNombreEnPosicion(posicion);
	}

	public void iniciarAlgoritmo() 
	{	
		if(agregarNuevoSeleccionado())
		{
			todas.add(Seleccion.leerJson(AgregarJugador.seleccionado));
		}
		todas.add(Seleccion.leerJson("Selecci�n Argentina"));
		todas.add(Seleccion.leerJson("Selecci�n Brasil"));
		todas.add(Seleccion.leerJson("Selecci�n Espa�ola"));
		todas.add(Seleccion.leerJson("Selecci�n Francesa"));
		todas.add(Seleccion.leerJson("Selecci�n Alemana"));
		
 
		@SuppressWarnings("unused")
		EquipoIdeal equipo = new EquipoIdeal(todas);	
	}

	public boolean agregarNuevoSeleccionado() 
	{
		if(AgregarJugador.seleccionado.equals(""))
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}

	public String dameCoeficiente() 
	{
		return String.valueOf(EquipoIdeal.coeficientEquipo());
	}

}
