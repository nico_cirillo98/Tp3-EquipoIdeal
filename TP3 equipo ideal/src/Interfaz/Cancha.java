package Interfaz;


import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import Presenter.presenter;
import javax.swing.JLabel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class Cancha extends JFrame {
	
	
	private static final long serialVersionUID = 1L;
	private Fondo contentPane;
	private presenter presenter= new presenter();

	public static void nuevaVentana() 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Cancha frame = new Cancha();
					frame.setVisible(true);
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	public Cancha() 
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(Cancha.class.getResource("/Interfaz/pelota.png")));
		setTitle("Equipo ideal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 904, 389);
		contentPane = new Fondo("cancha.jpg");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel arquero = new JLabel("");
		arquero.setBounds(39, 163, 101, 25);
		contentPane.add(arquero);
		
		JLabel lateralIzquierdo = new JLabel();
		lateralIzquierdo.setBounds(225, 26, 101, 25);
		contentPane.add(lateralIzquierdo);
		
		JLabel centralIzquierdo = new JLabel("");
		centralIzquierdo.setBounds(206, 108, 101, 25);
		contentPane.add(centralIzquierdo);
		
		JLabel centralDerecho = new JLabel("");
		centralDerecho.setBounds(206, 210, 101, 25);
		contentPane.add(centralDerecho);
		
		JLabel lateralDerecho = new JLabel("");
		lateralDerecho.setBounds(225, 301, 101, 25);
		contentPane.add(lateralDerecho);
		
		JLabel volanteIzquierdo = new JLabel("");
		volanteIzquierdo.setBounds(395, 75, 101, 25);
		contentPane.add(volanteIzquierdo);
		
		JLabel volanteCentral = new JLabel("");
		volanteCentral.setBounds(313, 163, 109, 25);
		contentPane.add(volanteCentral);
		
		JLabel volanteDerecho = new JLabel("");
		volanteDerecho.setBounds(395, 254, 101, 25);
		contentPane.add(volanteDerecho);
		
		JLabel punteroIzquierdo = new JLabel("");
		punteroIzquierdo.setBounds(550, 43, 101, 25);
		contentPane.add(punteroIzquierdo);
		
		JLabel centroDelantero = new JLabel("");
		centroDelantero.setBounds(650, 163, 101, 25);
		contentPane.add(centroDelantero);
		
		JLabel punteroDerecho = new JLabel("");
		punteroDerecho.setBounds(563, 290, 101, 25);
		contentPane.add(punteroDerecho);
		
		JButton botonCrear = new JButton("Crear equipo");
		botonCrear.setBounds(771, 338, 127, 23);
		contentPane.add(botonCrear);
		
		JLabel coefFinal = new JLabel("");
		coefFinal.setBounds(747, 11, 139, 25);
		contentPane.add(coefFinal);
		botonCrear.addActionListener(new ActionListener() 
		{
			
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				presenter.iniciarAlgoritmo();
				arquero.setText(presenter.setEquipo("Arquero"));
				lateralIzquierdo.setText(presenter.setEquipo("Lateral izquierdo"));
				lateralDerecho.setText(presenter.setEquipo("Lateral derecho"));
				centralDerecho.setText(presenter.setEquipo("Central derecho"));
				centralIzquierdo.setText(presenter.setEquipo("Central izquierdo"));
				volanteDerecho.setText(presenter.setEquipo("Volante derecho"));
				volanteIzquierdo.setText(presenter.setEquipo("Volante izquierdo"));
				volanteCentral.setText(presenter.setEquipo("Volante central"));
				punteroDerecho.setText(presenter.setEquipo("Puntero derecho"));
				punteroIzquierdo.setText(presenter.setEquipo("Puntero izquierdo"));
				centroDelantero.setText(presenter.setEquipo("Centro delantero"));
				coefFinal.setText("Coeficiente final:   "+presenter.dameCoeficiente());
				
			}
		});
				
	}
}
