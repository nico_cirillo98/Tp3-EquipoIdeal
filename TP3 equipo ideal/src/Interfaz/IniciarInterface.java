package Interfaz;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IniciarInterface extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textoPreg;

	public static void main(String[] args) 
	{
		try 
		{
			UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
			IniciarInterface dialog = new IniciarInterface();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public IniciarInterface() 
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(IniciarInterface.class.getResource("/Interfaz/pelota.png")));
		setTitle("Agregar Seleccion");
		setBounds(100, 100, 450, 230);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		textoPreg = new JTextField();
		textoPreg.setEditable(false);
		textoPreg.setHorizontalAlignment(SwingConstants.CENTER);
		textoPreg.setText("Desea crear una nueva seleccion?");
		textoPreg.setBounds(41, 46, 362, 57);
		contentPanel.add(textoPreg);
		textoPreg.setColumns(10);
		
		JButton botonSi = new JButton("Si");
		botonSi.setBounds(65, 152, 89, 23);
		contentPanel.add(botonSi);
		botonSi.addActionListener(new ActionListener() 
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				AgregarJugador selec= new AgregarJugador();
				selec.abrirVentana();
				dispose();
				
			}
		});
		
		JButton botonNo = new JButton("No");
		botonNo.setBounds(256, 152, 89, 23);
		contentPanel.add(botonNo);
		botonNo.addActionListener(new ActionListener() 
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
				Cancha.nuevaVentana();
			}
		});
		
	}
}
