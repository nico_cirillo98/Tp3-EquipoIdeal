package Interfaz;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import Presenter.presenter;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AgregarJugador extends JDialog {

	presenter pres= new presenter();
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField seleccion;
	private JTextField nombre;
	private JTextField posicion1;
	private JTextField posicion2;
	private JTextField goles;
	private JTextField faltas;
	private JTextField tarjetas;
	private JTextField puntajeProm;
	private Integer cont=1;
	public static String seleccionado="";

	public void abrirVentana() {
		try {
			AgregarJugador dialog = new AgregarJugador();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public AgregarJugador() {
		setTitle("Agregar seleccion");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AgregarJugador.class.getResource("/Interfaz/pelota.png")));
		setBounds(100, 100, 542, 382);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel infoSeleccion = new JLabel("Ingrese el pais de origen de la seleccion:");
		infoSeleccion.setBounds(10, 11, 218, 24);
		contentPanel.add(infoSeleccion);
		
		JLabel infoNombreJugador = new JLabel("Nombre del jugador:");
		infoNombreJugador.setBounds(10, 64, 218, 24);
		contentPanel.add(infoNombreJugador);
		
		JLabel infoPosicionJugador = new JLabel("Posicion/posiciones del jugador:");
		infoPosicionJugador.setBounds(10, 99, 218, 24);
		contentPanel.add(infoPosicionJugador);
		
		JLabel infoGoles = new JLabel("Goles convertidos:");
		infoGoles.setBounds(10, 152, 218, 24);
		contentPanel.add(infoGoles);
		
		JLabel infoFaltas = new JLabel("Faltas cometidas:");
		infoFaltas.setBounds(10, 187, 218, 24);
		contentPanel.add(infoFaltas);
		
		JLabel infoTarjetas = new JLabel("Tarjetas recibidas:");
		infoTarjetas.setBounds(10, 222, 218, 24);
		contentPanel.add(infoTarjetas);
		
		JLabel infoPuntajeProm = new JLabel("Puntaje promedio:");
		infoPuntajeProm.setBounds(10, 257, 218, 24);
		contentPanel.add(infoPuntajeProm);
		
		seleccion = new JTextField();
		seleccion.setBounds(257, 11, 232, 22);
		contentPanel.add(seleccion);
		seleccion.setColumns(10);
		
		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(257, 66, 232, 22);
		contentPanel.add(nombre);
		
		posicion1 = new JTextField();
		posicion1.setColumns(10);
		posicion1.setBounds(257, 101, 232, 22);
		contentPanel.add(posicion1);
		
		posicion2 = new JTextField();
		posicion2.setColumns(10);
		posicion2.setBounds(257, 123, 232, 22);
		contentPanel.add(posicion2);
		
		goles = new JTextField();
		goles.setColumns(10);
		goles.setBounds(257, 154, 232, 22);
		contentPanel.add(goles);
		
		faltas = new JTextField();
		faltas.setColumns(10);
		faltas.setBounds(257, 189, 232, 22);
		contentPanel.add(faltas);
		
		tarjetas = new JTextField();
		tarjetas.setColumns(10);
		tarjetas.setBounds(257, 224, 232, 22);
		contentPanel.add(tarjetas);
		
		puntajeProm = new JTextField();
		puntajeProm.setColumns(10);
		puntajeProm.setBounds(257, 259, 232, 22);
		contentPanel.add(puntajeProm);
		
		JButton botonEquipoIdeal = new JButton("Crear equipo ideal");
		botonEquipoIdeal.setEnabled(false);
		botonEquipoIdeal.setBounds(386, 320, 140, 23);
		contentPanel.add(botonEquipoIdeal);
		botonEquipoIdeal.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Cancha.nuevaVentana();
			}
		});
		
		JButton botonCrearJugador = new JButton("Crear jugador");
		botonCrearJugador.setBounds(174, 292, 133, 23);
		contentPanel.add(botonCrearJugador);
		botonCrearJugador.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				
				seleccionado=seleccion.getText();
				pres.agregarJugador(seleccion.getText(),nombre.getText(),posicion1.getText(),posicion2.getText(),Integer.parseInt(goles.getText()),Integer.parseInt(faltas.getText()),Integer.parseInt(tarjetas.getText()),Integer.parseInt(puntajeProm.getText()));
				seleccion.setEditable(false);
				//nombre.setText("");posicion1.setText("");posicion2.setText("");goles.setText("");faltas.setText("");puntajeProm.setText("");tarjetas.setText("");
				botonCrearJugador.setEnabled(pres.controlarContador(cont));
				Boolean control=pres.controlarContador(cont);
				cont++;
				botonEquipoIdeal.setEnabled(pres.habilitar(control));
				
			
				
			}
		});
	}
}
